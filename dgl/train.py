# ------------------
# FILE DESCRIPTION
# ------------------

"""
File : train.py
Author : Billy Carson
Date : 01-25-2021
Last modified : 02-06-2021

Description : Function definitions for training PyTorch models.
"""


# ----------------
# IMPORT MODULES
# ----------------

# Import modules
import sys
import numpy as np
from scipy.stats import sem
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import torch
from torch.utils.data import DataLoader
from torch.cuda.amp import GradScaler, autocast
import dgl
from dgl.data.utils import Subset

# Import model reset function
from load_data import *
# from model import reset_model_params


# ----------------------
# FUNCTION DEFINITIONS
# ----------------------

# Train model
def train_model(model, loss_fn, optimizer, train_loader, val_loader, device, n_epochs=100,
                checkpoint_path=None, amp=False, progress_step=10, verbose=2, print_indent=''):
    """
    Description
    -----------
    Model training function.
    
    Parameters
    ----------
    model : torch.nn.Module; model to be evaluated
    loss_fn : toch.nn.modules.loss; loss criterion
    optimizer : torch.optim.Optimizer; optimization instantiation
    train_loader : torch.utils.data.DataLoader; data loader instantiation for training set
    val_loader : torch.utils.data.DataLoader; data loader instantiation for training set
    device : str; device on which to put computations
    n_epochs : int; number of training epochs
    checkpoint_path : str; model checkpoint save path
    amp : bool; whether to use automatic mixed precision training
    progress_step : int; number of steps between displaying training progress
    verbose : int; determines how much output is displayed during training
    print_indent : str; indentation preceeding print statements
    
    Returns
    -------
    model : torch.nn.Module; trained PyTorch model
    """
    
    # Instantiate gradient scaler
    if amp:
        grad_scaler = GradScaler()
    
    # Initialize max metric variables
    train_acc_max = 0.0
    train_loss_min = sys.maxsize
    val_acc_max = 0.0
    val_loss_min = sys.maxsize

    # Iterate over number of training epochs
    for epoch in range(n_epochs):
        # Initialize variables
        # train_loss_list = []
        loss_sum = 0.0
        correct_pred = 0
        n_samples = 0

        # Put model in training mode
        model.train()

        # Train model
        for batch_id, batch_data in enumerate(train_loader):
            # Unpack batch data
            graph_batch, labels_batch = batch_data
            
            # Extract node features
            node_feat_batch = graph_batch.ndata.pop('node_attr').to(torch.double)
            
            node_feat_batch = node_feat_batch.to(torch.double)

            # Put graphs, features, and labels on GPU
            graph_batch = graph_batch.to(device)
            node_feat_batch, labels_batch = node_feat_batch.to(device), labels_batch.to(device).squeeze(-1)
            
            # Zero gradients
            optimizer.zero_grad()

            # Forward pass through model
            output = model(graph_batch, node_feat_batch)

            # Calculate loss
            loss = loss_fn(output, labels_batch)
                    
            # Mixed-precision backpropagation step
            if amp:
                grad_scaler.scale(loss).backward()
                grad_scaler.step(optimizer)
                grad_scaler.update()
            
            # Normal backpropagation step
            else:
                loss.backward()
                optimizer.step()

            # Update metrics variables
            correct_pred += (output.argmax(1)==labels_batch.long()).float().sum().item()
            n_batch = len(labels_batch)
            n_samples += n_batch
            loss_sum += n_batch * loss.item()

        # Calculate training metrics
        train_loss = loss_sum / n_samples
        train_acc = correct_pred / n_samples
        
        # Update max train accuracy
        if train_acc > train_acc_max:
            train_acc_max = train_acc
        
        # Update min train loss
        if train_loss < train_loss_min:
            train_loss_min = train_loss
    
        # Evaluate model on validation set
        val_acc, val_loss, _ = eval_model(model=model, 
                                          loss_fn=loss_fn,
                                          data_loader=val_loader,
                                          device=device,
                                          checkpoint_path=checkpoint_path)

        # Update max validation accuracy
        if (val_acc > val_acc_max) | ((val_acc == val_acc_max) & (val_loss < val_loss_min)):
            # Save model
            if checkpoint_path is not None:
                torch.save(model.state_dict(), checkpoint_path)

            # Update max validation accuracy
            val_acc_max = val_acc
        
        # Update min validation loss
        if val_loss < val_loss_min:
            val_loss_min = val_loss

        # Display training progress
        if verbose == 2:
            if (((epoch + 1) % progress_step) == 0) | (epoch == 0) | ((epoch + 1) == n_epochs):
                print('%sEpoch %05d | train loss: %s | val. loss: %s | train acc.: %s | val acc.: %s' % 
                      (print_indent, epoch + 1, '{:.4f}'.format(train_loss), '{:.4f}'.format(val_loss),
                       '{:.4f}'.format(train_acc), '{:.4f}'.format(val_acc)))
                if (epoch + 1) == n_epochs:
                    print('')
    
    # Display training results
    print('%sTraining results:\n' % (print_indent))
    print('%sTrain loss min:  %.5f' % (print_indent, train_loss_min))
    print('%sTrain acc. max:  %.5f' % (print_indent, train_acc_max))
    print('%sVal. loss min:   %.5f' % (print_indent, val_loss_min))
    print('%sVal. acc. max:   %.5f\n' % (print_indent, val_acc_max))
                
    # Load model checkpoint
    if checkpoint_path is not None:
        # model = model.load_state_dict(torch.load(checkpoint_path))
        model.load_state_dict(torch.load(checkpoint_path))

    # Return model
    return model


# Evaluation function
def eval_model(model, loss_fn, data_loader, device, checkpoint_path=None):
    """
    Description
    -----------
    Model evaluation function.
    
    Parameters
    ----------
    model : torch.nn.Module; model to be evaluated
    loss_fn : toch.nn.modules.loss; loss criterion
    data_loader : torch.utils.data.DataLoader; data loader instantiation
    device : str; device on which to put computations
    checkpoint_path : str; model checkpoint save path
    
    Returns
    -------
    acc : float; model accuracy on evaluation set
    loss : float; model loss on evaluation set
    (pred, labels) : tuple; model predictions and labels for evaluation set
    """
    
    # Initialize variables
    loss_sum = 0.0
    correct_pred = 0
    n_samples = 0
    
    # Initialize list of labels and predictions
    labels = []
    pred = []

    # Put model in evaluation mode
    model.eval()

    # Evaluate model on 
    for batch_id, batch_data in enumerate(data_loader):
        # Unpack batch data
        graph_batch, labels_batch = batch_data

        # Extract node features
        node_feat_batch = graph_batch.ndata.pop('node_attr')
            
        node_feat_batch = node_feat_batch.to(torch.double)

        # Put graphs, features, and labels on GPU
        graph_batch = graph_batch.to(device)
        node_feat_batch, labels_batch = node_feat_batch.to(device), labels_batch.to(device).squeeze(-1)

        # Forward pass through model
        output = model(graph_batch, node_feat_batch)

        # Backpropagation and parameter update
        loss = loss_fn(output, labels_batch)

        # Calculate number of correct predictions and total samples
        pred_batch = output.argmax(1)
        correct_pred += (pred_batch==labels_batch.long()).float().sum().item()
        n_batch = len(labels_batch)
        n_samples += n_batch
        loss_sum += n_batch * loss.item()
        
        # Append labels and predictions
        labels += list(labels_batch.cpu().numpy().ravel())
        pred += list(pred_batch.cpu().numpy().ravel())

    # Calculate validation metrics
    loss = loss_sum / n_samples
    acc = correct_pred / n_samples
    
    # Return evaluation results
    return acc, loss, (pred, labels)


# Cross-validation
def cross_val(model_fn, dataset, loss_fn, optimizer_class, kfold_splits, collate_func, device, n_folds=10,
              n_epochs=100, n_reps=None, lr=1e-3, batch_size=32, checkpoint_path=None, amp=False, progress_step=10,
              verbose=2, print_indent=''):
    """
    Description
    -----------
    K-fold cross-validation function.
    
    Parameters
    ----------
    model_fn : function; model instantiation function
    dataset : custom DGL dataset; graph dataset object
    loss_fn : toch.nn.modules.loss; loss criterion
    optimizer_class : class; uninstantiated optimizer class
    kfold_splits : list; list of dictionaries of cross-validation splits
    collate_func : function; graph dataset collation function
    device : str; device on which to put computations
    n_folds : int; number of cross-validation folds
    n_epochs : int; number of training epochs
    n_reps : int; number of k-fold cross-validation repetitions
    lr : float; learning rate
    batch_size : int; batch size
    checkpoint_path : str; model checkpoint save path
    amp : bool; whether to use automatic mixed precision training
    progress_step : int; number of steps between displaying training progress
    verbose : int; determines how much output is displayed during training
    print_indent : str; indentation preceeding print statements
    
    Returns
    -------
    N/A
    """
    
    # Repetitions
    if (n_reps is not None) & (n_reps != 0):
        # Initialize repetition metrics arrays
        rep_acc_arr = np.zeros(n_reps)
        rep_precision_arr = np.zeros(n_reps)
        rep_recall_arr = np.zeros(n_reps)
        rep_f1_arr = np.zeros(n_reps)
        
        # Iterate over repetitions
        for rep in range(n_reps):
            # Display repetition
            print('Repetition %d:\n' % (rep + 1))
            
            # Perform cross-validation
            agg_test_pred, agg_test_labels = _cross_val(model_fn=model_fn,
                                                        dataset=dataset,
                                                        loss_fn=loss_fn,
                                                        optimizer_class=optimizer_class,
                                                        kfold_splits=kfold_splits,
                                                        collate_func=collate_func,
                                                        device=device,
                                                        n_folds=n_folds,
                                                        n_epochs=n_epochs,
                                                        lr=lr,
                                                        batch_size=batch_size,
                                                        checkpoint_path=checkpoint_path,
                                                        amp=amp,
                                                        progress_step=progress_step,
                                                        verbose=verbose,
                                                        print_indent='  ')

            # Calculate aggregate test set metrics
            agg_test_acc = accuracy_score(y_true=agg_test_labels, y_pred=agg_test_pred)
            agg_test_precision = precision_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')
            agg_test_recall = recall_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')
            agg_test_f1 = f1_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')
            
            # Populate aggregate test set metrics arrays
            rep_acc_arr[rep] = agg_test_acc
            rep_precision_arr[rep] = agg_test_precision
            rep_recall_arr[rep] = agg_test_recall
            rep_f1_arr[rep] = agg_test_f1
            
            # Report cross-validation results
            if (verbose == 1) | (verbose == 2):
                print('  K-fold aggregate test set results:\n')
                print('  Acc.:       %.5f' % (agg_test_acc))
                print('  Precision:  %.5f' % (agg_test_precision))
                print('  Recall:     %.5f' % (agg_test_recall))
                print('  F1:         %.5f\n' % (agg_test_f1))
        
        # Report repetition results
        if (verbose == 1) | (verbose == 2):
            print('Repetition test set results:\n')
            print('Acc.:       %.5f +/- %.5f' % (np.mean(rep_acc_arr), sem(rep_acc_arr)))
            print('Precision:  %.5f +/- %.5f' % (np.mean(rep_precision_arr), sem(rep_precision_arr)))
            print('Recall:     %.5f +/- %.5f' % (np.mean(rep_recall_arr), sem(rep_recall_arr)))
            print('F1:         %.5f +/- %.5f\n' % (np.mean(rep_f1_arr), sem(rep_f1_arr)))
    
    # No repetitions:
    else:
        # Perform cross-validation
        agg_test_pred, agg_test_labels = _cross_val(model=model,
                                                    dataset=dataset,
                                                    loss_fn=loss_fn,
                                                    optimizer_class=optimizer_class,
                                                    kfold_splits=kfold_splits,
                                                    collate_func=collate_func,
                                                    device=device,
                                                    n_folds=n_folds,
                                                    n_epochs=n_epochs,
                                                    lr=lr,
                                                    batch_size=batch_size,
                                                    checkpoint_path=checkpoint_path,
                                                    amp=amp,
                                                    progress_step=progress_step,
                                                    verbose=verbose,
                                                    print_indent='')

        # Calculate aggregate test set metrics
        agg_test_acc = accuracy_score(y_true=agg_test_labels, y_pred=agg_test_pred)
        agg_test_precision = precision_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')
        agg_test_recall = recall_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')
        agg_test_f1 = f1_score(y_true=agg_test_labels, y_pred=agg_test_pred, average='macro')

        # Report cross-validation results
        if (verbose == 1) | (verbose == 2):
            print('K-fold aggregate test set results:\n')
            print('Acc.:       %.5f' % (agg_test_acc))
            print('Precision:  %.5f' % (agg_test_precision))
            print('Recall:     %.5f' % (agg_test_recall))
            print('F1:         %.5f\n' % (agg_test_f1))


# Cross-validation helper function
def _cross_val(model_fn, dataset, loss_fn, optimizer_class, kfold_splits, collate_func, device, n_folds=10,
               n_epochs=100, lr=1e-3, batch_size=32, checkpoint_path=None, amp=False, progress_step=10,
               verbose=2, print_indent=''):
    """
    Description
    -----------
    K-fold cross-validation helper function
    
    Parameters
    ----------
    model_fn : function; model instantiation function
    dataset : custom DGL dataset; graph dataset object
    loss_fn : toch.nn.modules.loss; loss criterion
    optimizer_class : class; uninstantiated optimizer class
    kfold_splits : list; list of dictionaries of cross-validation splits
    collate_func : function; graph dataset collation function
    device : str; device on which to put computations
    n_folds : int; number of cross-validation folds
    n_epochs : int; number of training epochs
    batch_size : int; batch size
    checkpoint_path : str; model checkpoint save path
    amp : bool; whether to use automatic mixed precision training
    progress_step : int; number of steps between displaying training progress
    verbose : int; determines how much output is displayed during training
    print_indent : str; indentation preceeding print statements
    
    Returns
    -------
    agg_test_pred : list; list of test set predictions
    agg_test_labels : list; list of test set labels
    """

    # Initialize aggregated test set metrics lists
    agg_test_pred = []
    agg_test_labels = []

    # Iterate over folds
    for fold in range(n_folds):
        # Display fold
        print('%sFold %d:\n' % (print_indent, fold + 1))

        # Split dataset
        # dataset_train = Subset(dataset, kfold_splits[fold]['train'])
        # dataset_val = Subset(dataset, kfold_splits[fold]['val'])
        # dataset_test = Subset(dataset, kfold_splits[fold]['test'])
        dataset_train = get_dataset_subset(dataset, kfold_splits[fold]['train'], remove_zero_in_degree_samples=True)
        dataset_val = get_dataset_subset(dataset, kfold_splits[fold]['val'], remove_zero_in_degree_samples=True)
        dataset_test = get_dataset_subset(dataset, kfold_splits[fold]['test'], remove_zero_in_degree_samples=True)

        # Instantiate data loaders
        train_loader = DataLoader(dataset_train, batch_size=batch_size, collate_fn=collate_func)
        val_loader = DataLoader(dataset_val, batch_size=batch_size, collate_fn=collate_func)
        test_loader = DataLoader(dataset_test, batch_size=batch_size, collate_fn=collate_func)
        
        # Instantiate model
        model = model_fn()
        
        model.to(torch.double)
        
        # Put model on GPU
        model.to(device)
        
        # Instantiate optimizer
        optimizer = optimizer_class(params=model.parameters(), lr=lr)

        # Train model
        model = train_model(model=model,
                            loss_fn=loss_fn,
                            optimizer=optimizer,
                            train_loader=train_loader,
                            val_loader=val_loader,
                            device=device,
                            n_epochs=n_epochs,
                            checkpoint_path=checkpoint_path,
                            amp=amp,
                            progress_step=progress_step,
                            verbose=verbose,
                            print_indent=print_indent + '  ')

        # Evaluate model on test set
        test_acc, test_loss, test_results_arr = eval_model(model=model,
                                                           loss_fn=loss_fn,
                                                           data_loader=test_loader,
                                                           device=device,
                                                           checkpoint_path=checkpoint_path)

        # Unpack test set results tuples
        test_pred = test_results_arr[0]
        test_labels = test_results_arr[1]

        # Append test set results to aggregate lists
        agg_test_pred += test_pred
        agg_test_labels += test_labels
        
        # Calculate test set metrics
        test_precision = precision_score(y_true=test_labels, y_pred=test_pred, average='macro')
        test_recall = recall_score(y_true=test_labels, y_pred=test_pred, average='macro')
        test_f1 = f1_score(y_true=test_labels, y_pred=test_pred, average='macro')
        
        # Report test set results
        if (verbose == 1) | (verbose == 2):
            print('%s  Fold %d test set results:\n' % (print_indent, fold + 1))
            print('%s  Acc.:       %.5f' % (print_indent, test_acc))
            print('%s  Precision:  %.5f' % (print_indent, test_precision))
            print('%s  Recall:     %.5f' % (print_indent, test_recall))
            print('%s  F1:         %.5f\n' % (print_indent, test_f1))
    
    # Return aggregate test result arrays
    return agg_test_pred, agg_test_labels

