# ------------------
# FILE DESCRIPTION
# ------------------

"""
File : load_data.py
Author : Billy Carson
Date : 01-25-2021
Last modified : 02-06-2021

Description : Data loading function and class definitions.
"""


# ----------------
# IMPORT MODULES
# ----------------

# Import modules
import numpy as np
import torch
import dgl
from dgl.data.utils import Subset


# ----------------------
# FUNCTION DEFINITIONS
# ----------------------

# # Dataset subset function
# def get_dataset_subset(dataset, idx):
#     """
#     Description
#     -----------
#     Dataset subset retrieval function.
    
#     Parameters
#     ----------
#     dataset : custom DGL dataset; graph dataset
#     idx : np.ndarray; subset indices
    
#     Returns
#     -------
#     dataset_subset : custom DGL dataset; subset of graph dataset
#     """
    
#     # Get subset of dataset
#     dataset_subset = Subset(dataset, idx)
    
#     # Return dataset subset
#     return dataset_subset


# # Remove samples with zero in-degree nodes
# def remove_zero_in_degree_samples(dataset):
#     """
#     Description
#     -----------
    
#     Parameters
#     ----------
    
#     Returns
#     -------
#     """
    
#     # Initialize list of indices
#     idx_remove_list = []
    
#     # Iterate over length of dataset
#     for i in range(len(dataset)):
#         # Retrieve graph
#         graph, _ = dataset[i]

#         # Iterate over graph nodes
#         for j in range(graph.num_nodes()):
#             # Check for zero in-degree nodes
#             if graph.in_degrees(j) == 0:
#                 # Append sample index to list
#                 idx_remove_list.append(i)
#                 break
    
#     # Create keep indices
#     idx_all = np.arange(0, len(dataset))
#     idx_remove = np.array(idx_remove_list).ravel()
#     idx_keep = np.setdiff1d(idx_all, idx_remove, assume_unique=True)
    
#     # Get subset with zero in-degree samples removed
#     dataset_zero_in_degree_removed = get_dataset_subset(dataset=dataset, idx=idx_keep)
    
#     # Return modified dataset
#     return dataset_zero_in_degree_removed


# Dataset subset function
def get_dataset_subset(dataset, idx, remove_zero_in_degree_samples=False):
    """
    Description
    -----------
    Dataset subset retrieval function.
    
    Parameters
    ----------
    dataset : custom DGL dataset; graph dataset
    idx : np.ndarray; subset indices
    
    Returns
    -------
    dataset_subset : custom DGL dataset; subset of graph dataset
    """
    
    # Remove samples with zero in-degree nodes
    if remove_zero_in_degree_samples:
        # Initialize list of indices
        idx_remove_list = []
        
        # Iterate over samples
        for i in list(idx):
            # Retrieve graph
            graph, _ = dataset[i]

            # Iterate over graph nodes
            for j in range(graph.num_nodes()):
                # Check for zero in-degree nodes
                if graph.in_degrees(j) == 0:
                    # Append sample index to list
                    idx_remove_list.append(i)
                    break

        # Create keep indices
        idx_remove = np.array(idx_remove_list).ravel()
        idx = np.setdiff1d(idx, idx_remove, assume_unique=True)
    
    # Get subset of dataset
    dataset_subset = Subset(dataset, idx)
    
    # Return dataset subset
    return dataset_subset


# Data collation function
def collate_graphs(data):
    """
    Description
    -----------
    Graph dataset collation function.
    
    Parameters
    ----------
    
    Returns
    -------
    """
    
    # Format graphs and labels
    graphs, labels = map(list, zip(*data))
    bg = dgl.batch(graphs)
    labels = torch.stack(labels, dim=0)
    
    # Return batch of graphs and labels
    return bg, labels


# ----------------------
# CLASS DEFINITIONS
# ----------------------

# K-fold object
class KFold():
    """
    Description
    -----------
    K-fold cross validation object. Returns a list of split indices for each fold. Each split contains
    approximately equal proportions of class indices as the original labels array.
    
    Parameters
    ----------
    labels : list or np.ndarray; categorical labels
    n_folds : int; number of cross-validation folds
    splits_as_arrays : bool; indicates whether to convert split indices to arrays or leave as lists
    random_state : int; random state seed
    
    Attributes
    ----------
    n_samp : int; length of labels
    n_folds : int; number of cross-validation folds
    kfold_idx : list; list of indices specific to a given fold
    splits : list; list of dictionary of k-fold cross-validation splits
    splits_as_arrays : bool; if true splits will be in np.ndarray format, otherwise splits will be lists
    random_state : int; random state seed
    labels : list or np.ndarray; categorical labels
    n_classes : int; number of classes represented in labels
    """
    
    # K-fold initialization method
    def __init__(self, labels, n_folds=10, splits_as_arrays=False, random_state=None):
        """
        Description
        -----------
        K-fold cross-validation object initialization method.

        Parameters
        ----------
        labels : list or np.ndarray; categorical labels
        n_folds : int; number of cross-validation folds
        splits_as_arrays : bool; if true splits will be in np.ndarray format, otherwise splits will be lists
        random_state : int; random state seed
        
        Returns
        -------
        """
        
        # Assign attributes
        self.n_samp = len(labels)
        self.n_folds = n_folds
        self.kfold_idx = None
        self.splits = None
        self.splits_as_arrays = splits_as_arrays
        self.random_state = random_state
        
        # Check dimensionality of labels array
        if len(labels.shape) > 1:
            if np.min(labels.shape) == 1:
                self.labels = labels.copy().ravel()
                self.n_classes = len(np.unique(self.labels))
            else:
                raise ValueError("Labels must be either a 1-dimensional array or a list. One-hot encoded labels are not supported.")
        else:
            self.labels = labels.copy()
            self.n_classes = len(np.unique(self.labels))
    
    # Generate K-fold splits
    def gen_splits(self):
        """
        Description
        -----------
        K-fold cross-validation splits generation method.

        Parameters
        ----------
        N/A

        Returns
        -------
        N/A
        """

        # Set numpy random seed
        if self.random_state is not None:
            np.random.seed(seed=self.random_state)
        
        # Initialize K-fold indices list
        kfold_idx = []
        for fold in range(self.n_folds):
            kfold_idx.append([])
        
        # Initialize K-fold train, validation, test splits list
        kfold_splits = []
        for fold in range(self.n_folds):
            kfold_splits.append({})

        # Initialize "big" fold remainder variable
        big_fold_idx_rem = np.arange(self.n_folds)

        # Iterate over class number
        for class_i in range(self.n_classes):
            # Get class indices
            class_idx = np.where(self.labels==class_i)[0]
            len_class_idx = len(class_idx)

            # Class indices equally divisible by number of folds
            fold_size_rem = len_class_idx % self.n_folds
            if fold_size_rem == 0:
                # Calculate fold size
                fold_size = int(len_class_idx / self.n_folds)

                # Assign fold indices
                for fold in range(self.n_folds):
                    # Get class indices for fold and update class index array
                    class_idx_subset = np.random.choice(class_idx, fold_size, replace=False)
                    class_idx = np.setdiff1d(class_idx, class_idx_subset, assume_unique=True)

                    # Append fold indices to fold index list
                    kfold_idx[fold] += list(class_idx_subset)

            # Class indices not equally divisible by number of folds
            else:
                # Number of "big" and "small" folds if remainder is non-zero
                n_big_folds = len_class_idx % self.n_folds
                n_small_folds = self.n_folds - n_big_folds

                # Sizes of "big" and "small" folds if remainder is non-zero
                big_fold_size = ceil(len_class_idx / self.n_folds)
                small_fold_size = int(len_class_idx / self.n_folds)

                # Determine which folds should have "big" number of samples
                if n_big_folds > len(big_fold_idx_rem):
                    big_fold_idx_new = np.random.choice(np.setdiff1d(np.arange(self.n_folds), big_fold_idx_rem, assume_unique=True),
                                                        n_big_folds, replace=False)
                    big_fold_idx = np.hstack((big_fold_idx_rem, big_fold_idx_new))
                    big_fold_idx_rem = np.setdiff1d(np.arange(self.n_folds), big_fold_idx_new, assume_unique=True)
                else:
                    big_fold_idx = np.random.choice(big_fold_idx_rem, n_big_folds, replace=False)
                    big_fold_idx_rem = np.setdiff1d(big_fold_idx_rem, big_fold_idx, assume_unique=True)

                # Get class indice subset
                for fold in range(self.n_folds):
                    if fold in (big_fold_idx):
                        class_idx_subset = np.random.choice(class_idx, big_fold_size, replace=False)
                        class_idx = np.setdiff1d(class_idx, class_idx_subset, assume_unique=True)
                    else:
                        class_idx_subset = np.random.choice(class_idx, small_fold_size, replace=False)
                        class_idx = np.setdiff1d(class_idx, class_idx_subset, assume_unique=True)

                    # Append fold indices to fold index list
                    kfold_idx[fold] += list(class_idx_subset)

        # Check that each label index is represented in the splits
        kfold_idx_unraveled = []
        for fold in range(self.n_folds):
            kfold_idx_unraveled += kfold_idx[fold]
        
        # Assertion that each label index is represented in the splits
        kfold_idx_unraveled.sort()
        assert(kfold_idx_unraveled == list(np.arange(len(self.labels))))
        
        # Assign generated K-fold indices to K-fold indices attribute
        self.kfold_idx = kfold_idx
        
        # Generate splits and assign to dictionary elements
        val_fold = 0
        test_fold = 1
        for fold_i in range(self.n_folds):
            train_idx = []
            val_idx = kfold_idx[val_fold]
            test_idx = kfold_idx[test_fold]
            for fold_j in range(self.n_folds):
                if (fold_j != val_fold) & (fold_j != test_fold):
                    train_idx += kfold_idx[fold_j]
            if self.splits_as_arrays:
                train_idx = np.array(train_idx)
                val_idx = np.array(val_idx)
                test_idx = np.array(test_idx)
            kfold_splits[fold_i]['train'] = train_idx
            kfold_splits[fold_i]['val'] = val_idx
            kfold_splits[fold_i]['test'] = test_idx
            val_fold += 1
            if test_fold + 1 >= self.n_folds:
                test_fold =0
            else:
                test_fold += 1
        
        # Assign generated K-fold splits to K-fold splits attribute
        self.splits = kfold_splits
    
    # Return K-fold splits
    def get_splits(self):
        """
        Description
        -----------
        K-fold cross-validation splits retrieval function.

        Parameters
        ----------
        N/A

        Returns
        -------
        self.splits : list; list of dictionaries of k-fold cross-validation splits
        """
        
        # Generate splits if splits attribute is not yet assigned
        if self.splits is None:
            self.gen_splits()
        
        # Return K-fold splits
        return self.splits

