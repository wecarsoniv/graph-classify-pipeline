# ------------------
# FILE DESCRIPTION
# ------------------

"""
File : model.py
Author : Billy Carson
Date : 01-22-2021
Last modified : 02-08-2021

Description : Established graph neural network model implementations.
"""


# ----------------
# IMPORT MODULES
# ----------------

# Import modules
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.cuda.amp import autocast
import dgl
import dgl.function as fn
from dgl.nn.pytorch import conv, edge_softmax, GATConv
        

# -------------------
# MODEL DEFINITIONS
# -------------------

# Kipf et al. (2017) - Graph Convolutional Networks
class GCN(nn.Module):
    """
    Description
    -----------
    Graph Convolutional Network (Kipf and Welling, 2017 - Semi-Supervised Classification with Graph Convolutional Networks).

    Parameters
    ----------
    input_node_dim : int; input node dimension
    latent_node_dim : int; latent node dimension
    output_dim : int; model output dimension
    amp : bool; whether to use automatic mixed precision training

    Attributes
    ----------
    input_node_dim : int; input node dimension
    latent_node_dim : int; latent node dimension
    output_dim : int; model output dimension
    amp : bool; whether to use automatic mixed precision training
    gcn_layers : torch.nn.ModuleList; module list of GCN layers
    classifier : torch.nn.Module; linear classifier layer
    """
    
    # Initialization method
    def __init__(self, input_node_dim, latent_node_dim, output_dim, n_layers=3, amp=False):
        """
        Description
        -----------
        Graph Convolutional Network initialization method.
        
        Parameters
        ----------
        input_node_dim : int; input node dimension
        latent_node_dim : int; latent node dimension
        output_dim : int; model output dimension
        amp : bool; whether to use automatic mixed precision training
        
        Returns
        -------
        N/A
        """
        
        # Inherit from parent module class
        super(GCN, self).__init__()
        
        # Assign attributes
        self.input_node_dim = input_node_dim
        self.latent_node_dim = latent_node_dim
        self.output_dim = output_dim
        self.n_layers = n_layers
        self.amp = amp
        
        # Graph convolution layers
        self.gcn_layers = nn.ModuleList()
        self.gcn_layers.append(conv.GraphConv(input_node_dim, latent_node_dim, activation=F.relu,
                                              allow_zero_in_degree=True))
        for l in range(1, n_layers):
            self.gcn_layers.append(conv.GraphConv(latent_node_dim, latent_node_dim, activation=F.relu,
                                                  allow_zero_in_degree=True))
        
        # Linear classifier
        # self.classifier = nn.Linear(latent_node_dim, output_dim)
        self.classifier = nn.Sequential(
            nn.Linear(latent_node_dim, 256),
            nn.Linear(256, output_dim))
    
    # Forward method
    def forward(self, g, node_feat):
        """
        Description
        -----------
        Graph Convolutional Network forward method.
        
        Parameters
        ----------
        g : DGLGraph; batch of graphs
        node_feat : torch.tensor; input node features
        
        Returns
        -------
        output : torch.tensor; model output
        """
        
        # Enable autocast
        with (autocast(enabled=self.amp) if self.training else autocast(enabled=False)):
            # Reassign node features
            h = node_feat
            
            # Forward pass
            for l in range(0, self.n_layers):
                # Get node latent representation
                h = self.gcn_layers[l](g, h)
                
                # Update node features
                ndata_key = 'conv' + str(l + 1) + '_feat'
                g.ndata[ndata_key] = h

            # Node readout function
            # h_readout = dgl.readout_nodes(graph=g, feat=g.ndata['conv3_feat'], weight=None, op='mean')
            h_readout = dgl.mean_nodes(g, ndata_key)

            # Get model output
            output = self.classifier(h_readout)

            # Return model output
            return output
    
    # Reset parameters
    def reset_params(self):
        """
        """
        pass


# Velickovic et al. (2018) - Graph Attention Networks
class GAT(nn.Module):
    """
    Description
    -----------
    Graph Attention Network (Velickovic et al., 2018 - Graph Attention Networks).

    Parameters
    ----------
    input_node_dim :
    latent_node_dim :
    n_classes :
    n_layers :
    n_heads :
    activation :
    feat_drop :
    attn_drop :
    neg_slope :
    residual :
    amp :

    Attributes
    ----------
    input_node_dim :
    latent_node_dim :
    n_classes :
    n_layers :
    n_heads :
    activation :
    feat_drop :
    attn_drop :
    neg_slope :
    residual :
    amp :
    gat_layers :
    """
    
    # Initialization method
    def __init__(self, input_node_dim, latent_node_dim, n_classes, n_layers=3, n_heads=8, activation=F.relu,
                 feat_drop=0.0, attn_drop=0.0, neg_slope=0.2, residual=False, amp=False):
        
        # Inherit from parent module class
        super(GAT, self).__init__()
        
        # Initialize attributes
        self.input_node_dim = input_node_dim
        self.latent_node_dim = latent_node_dim
        self.n_classes = n_classes
        self.n_layers = n_layers
        self.n_heads = n_heads
        self.activation = activation
        self.feat_drop = feat_drop
        self.attn_drop = attn_drop
        self.neg_slope = neg_slope
        self.residual = residual
        self.amp = amp
        
        # Initialize module list of GAT layers
        self.gat_layers = nn.ModuleList()
        
        # Input projection (no residual)
        # self.gat_layers.append(GATConv(in_feats=input_node_dim, out_feats=latent_node_dim, num_heads=heads[0],
        #                                feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
        #                                residual=False, activation=activation))
        self.gat_layers.append(GATConv(in_feats=input_node_dim, out_feats=latent_node_dim, num_heads=n_heads,
                                       feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
                                       residual=False, activation=activation))
        
        # Hidden layers
        for l in range(1, n_layers -1):
            # due to multi-head, the in_dim = num_hidden * num_heads
            # self.gat_layers.append(GATConv(in_feats=num_hidden * heads[l - 1], out_feats=num_hidden, num_heads=heads[l],
            #                                feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
            #                                residual=residual, activation=activation))
            self.gat_layers.append(GATConv(in_feats=latent_node_dim * n_heads, out_feats=latent_node_dim, num_heads=n_heads,
                                           feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
                                           residual=residual, activation=activation))
        
        # Output projection
        # self.gat_layers.append(GATConv(in_feats=num_hidden * heads[-2], out_feats=n_classes, num_heads=heads[-1],
        #                                feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
        #                                residual=residual, activation=None))
        self.gat_layers.append(GATConv(in_feats=latent_node_dim * n_heads, out_feats=latent_node_dim, num_heads=1,
                                       feat_drop=feat_drop, attn_drop=attn_drop, negative_slope=neg_slope,
                                       residual=residual, activation=None))

        # Classifier
        self.classifier = nn.Sequential(
            nn.Linear(latent_node_dim, 256),
            nn.Linear(256, n_classes))
    
    # Forward method
    def forward(self, g, node_feat):
        """
        Description
        -----------
        Graph Attention Network forward method.
        
        Parameters
        ----------
        g : DGLGraph; batch of graphs
        node_feat : torch.tensor; input node features
        
        Returns
        -------
        logits : torch.tensor; model output logits
        """
        
        # Enable autocast
        with (autocast(enabled=self.amp) if self.training else autocast(enabled=False)):
            # Reassign node features
            h = node_feat

            # Forward pass
            for l in range(self.n_layers):
                # Get latent node representation
                h = self.gat_layers[l](graph=g, feat=h).flatten(1)

                # Update node features
                ndata_key = 'gat' + str(l + 1) + '_feat'
                g.ndata[ndata_key] = h

            # Node readout function
            # h_readout = dgl.readout_nodes(graph=g, feat=g.ndata['conv3_feat'], weight=None, op='mean')
            h_readout = dgl.mean_nodes(g, ndata_key)

            # Output projection
            # logits = self.gat_layers[-1](graph=g, feat=h).mean(1)

            # Output projection
            logits = self.classifier(h_readout)

            # Return model output logits
            return logits

